/**
 * Implements the Contact interface
 *
 * @author Julian Fenner
*/
public class ContactImpl implements Contact{
	private int contactId;
	private String name;
	private String notes;
	
	public ContactImpl (String name, int contactId){
		this(name, "", contactId);
	}
	
	public ContactImpl (String name, String notes, int contactId){
		this.name = name;
		this.contactId = contactId;
		this.notes = notes;
	}
	
	@Override
	public int getId(){
		return contactId;
	}
	
	@Override
	public String getName(){
		return name;
	}
	
	@Override
	public String getNotes(){
		return notes;
	}
	
	@Override
	public void addNotes (String note){
		this.notes += note;
	}

}