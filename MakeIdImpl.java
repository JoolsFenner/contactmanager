/**
 * Implements the MakeId interface.
 *
 * @author Julian Fenner
*/

public class MakeIdImpl implements MakeId{
	private static int id = 0;
	
	@Override 
	public int createNewId (){
		id ++;
		return id;	
	}
	
	@Override 
	public void reInitialiseId(int savedId){
		this.id = savedId;
	}
	
	@Override 
	public int getId(){
		return id;
	}
}