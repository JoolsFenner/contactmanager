import java.util.Set;
import java.util.Calendar;


/**
 * Implements the PastMeeting interface
 *
 * @author Julian Fenner
*/
 public class PastMeetingImpl extends MeetingImpl implements PastMeeting {
	private String notes;
	
	public PastMeetingImpl(Set<Contact> meetingParticipants, Calendar calendar, int meetingId, String notes) {	
		super(meetingParticipants, calendar, meetingId);
		this.notes = notes;
	}
	
	@Override
	public String getNotes(){
		return notes;
	}
	
	/**
	*
	*For adding additional notes to past meeting
	*
	*@param notes to be added
	*/
	public void addAdditionalNotes(String text){
		this.notes += ". " + text;
	}
}