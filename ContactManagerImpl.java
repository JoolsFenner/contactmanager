import java.util.Calendar;
import java.util.GregorianCalendar;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.io.*;
import java.util.Comparator;
import java.util.Collections;



/**
 * Implements the ContactManager interface
 *
 * @author Julian Fenner
*/
public class ContactManagerImpl implements ContactManager{

	private MakeId idGenerator;
	private final File SAVED_DATA;

	private Map<Integer, Meeting> meetingsMap;
	private Map<Integer, Contact> contactsMap;
	
	private Calendar now;
	
	/**
	 * Constructor requires file name for data file.  If none entered contacts.txt assigned by default
	 *
	 *@param savedDataFileName is name of user's dataFile.  
	 *
	*/
	public ContactManagerImpl(String savedDataFileName){
		idGenerator = new MakeIdImpl();
		meetingsMap = new HashMap<Integer, Meeting>();
		contactsMap = new HashMap<Integer, Contact>();
		SAVED_DATA = new File(savedDataFileName);
		now = Calendar.getInstance();
		
		if(fileExistsAndContainsData()) loadSavedData(); 
	}
	
	public ContactManagerImpl(){
		this("contacts.txt");
	}
	
	@Override
	public int addFutureMeeting(Set<Contact> contacts, Calendar date){
		if(date.before(now)) throw new IllegalArgumentException("Error. This meeting is in the past");
		if(!allContactsInSet(contacts)) throw new IllegalArgumentException("Contact in list doen't exist");
		
		Meeting meeting = new FutureMeetingImpl(contacts, date, idGenerator.createNewId());
		meetingsMap.put(meeting.getId(), meeting);
		return meeting.getId();
	}
	
	
	@Override
	public PastMeeting getPastMeeting(int id){
		if(meetingsMap.get(id) ==  null) return null;
		if(meetingsMap.get(id).getDate().after(now)) throw new IllegalArgumentException("Error. This meeting is in the future");
		return (PastMeeting) meetingsMap.get(id);
	}
	
	
	@Override
	public FutureMeeting getFutureMeeting(int id){
		if(meetingsMap.get(id) ==  null) return null;
		if(meetingsMap.get(id).getDate().before(now)) throw new IllegalArgumentException("Error. This meeting is in the past");
		return (FutureMeeting) meetingsMap.get(id);
	}

	
	@Override
	public Meeting getMeeting(int id){
		if(meetingsMap.get(id) ==  null) return null;
		return meetingsMap.get(id);
	}

	
	@Override	
	public List<Meeting> getFutureMeetingList(Contact contact){
		if(contactsMap.get(contact.getId()) == null) throw new IllegalArgumentException("Error. This contact does not exist");
		
		List<Meeting> futureMeetingList = new ArrayList<Meeting>();
		for(Meeting meeting : meetingsMap.values()){
			if(contactInMeeting(meeting.getContacts(), contact) && meeting.getDate().after(now)){
				futureMeetingList.add((MeetingImpl) meeting);	
			}
		}		
		
		Collections.sort(futureMeetingList, new Comparator<Meeting>(){
			@Override
			public int compare(Meeting m1, Meeting m2){
				return m1.getDate().compareTo(m2.getDate());
			}
		});
	
		return futureMeetingList;
	}

	
	@Override	
	public List<Meeting> getFutureMeetingList(Calendar date){
		List<Meeting> futureMeetingList = new ArrayList<Meeting>();
		
		for(Meeting meeting : meetingsMap.values()){
			if(daysMatch(date, meeting.getDate())) futureMeetingList.add(meeting);
		}
			
		return futureMeetingList;
	}

	
	@Override
	public List<PastMeeting> getPastMeetingList(Contact contact){
		if(contactsMap.get(contact.getId()) == null) throw new IllegalArgumentException("Error. This contact does not exist");

		List<PastMeeting> pastMeetingList = new ArrayList<PastMeeting>();
		for(Meeting meeting : meetingsMap.values()){
			if(contactInMeeting(meeting.getContacts(), contact) && meeting.getDate().before(now)){
			pastMeetingList.add((PastMeeting)meeting);}
		}		
		
		Collections.sort(pastMeetingList, new Comparator<Meeting>(){
			@Override
			public int compare(Meeting m1, Meeting m2){
				return m1.getDate().compareTo(m2.getDate());
			}
		});
		
		return pastMeetingList;
	}

	
	@Override
	public void addNewPastMeeting(Set<Contact> contacts, Calendar date, String text) throws NullPointerException{
		if(contacts.isEmpty()) throw new IllegalArgumentException("Error, no contacts in the set");
		if (!allContactsInSet(contacts)) throw new IllegalArgumentException("Error, contact doesn't exist");
	
		if(date.after(now)) {
			throw new IllegalArgumentException("Error. This meeting is in the past");	
		} 
		
		PastMeeting meeting = new PastMeetingImpl(contacts, date, idGenerator.createNewId(), text);
		meetingsMap.put(meeting.getId(), meeting);
	}
	
	
	@Override
	public void addMeetingNotes(int id, String text)  {
		if(text == null) throw new NullPointerException("Error, notes are null");
		if(meetingsMap.get(id) == null) throw new IllegalArgumentException("Error. Meeting ID " + id + " does not exist");
		if(meetingsMap.get(id).getDate().after(now)) throw new IllegalStateException("Error. This meeting is in the future");
		
		
		if(meetingsMap.get(id) instanceof PastMeeting){
			// i.e if already a pastMeeting
			PastMeetingImpl tempPastMeeting = (PastMeetingImpl) meetingsMap.get(id);
			tempPastMeeting.addAdditionalNotes(text);
			return;
		} else {
			//if currently futureMeeting can't downcast so need to save values, add to new instance and delete old object
			Meeting tempMeeting = meetingsMap.get(id); 
			int tempMeetingId = tempMeeting.getId();
			Calendar tempMeetingDate = tempMeeting.getDate();
			Set<Contact> tempMeetingContacts = tempMeeting.getContacts();
			
			meetingsMap.remove(id);
			
			Meeting convertedMeeting = new PastMeetingImpl(tempMeetingContacts, tempMeetingDate, tempMeetingId, text);
			meetingsMap.put(convertedMeeting.getId(), convertedMeeting);
		}
	}


	@Override
	public void addNewContact(String name, String notes){
		Contact contact = new ContactImpl(name, notes, idGenerator.createNewId());
		if(notes==null || name == null) throw new NullPointerException("Error, notes are empty");
		contactsMap.put(contact.getId(), contact);
	}

	
	@Override
	public Set<Contact> getContacts(int... ids){
		Set<Contact> contactSet = new HashSet<Contact>();
		
		for (int id: ids){
			if (contactsMap.get(id) == null) throw new IllegalArgumentException("Error. Contact ID " + id + " does not exist");
			contactSet.add(contactsMap.get(id));
		}
		return contactSet;
	}

	
	@Override
	public Set<Contact> getContacts(String name){
		if(name == null) {
			throw new NullPointerException("Error. Null string");	
		} 
		
		Set<Contact> contactSet = new HashSet<Contact>();
		
		for(Contact contact : contactsMap.values()){
			if(contact.getName().toLowerCase().contains(name.toLowerCase())){
				contactSet.add(contact);
			}
		}
		return contactSet;
	}

	
	
	@Override
	public void flush(){
		FileWriter fwrite = null;
		PrintWriter out = null;
		DateFormat dateFormat = new SimpleDateFormat("yyyy,MM,dd");
		
		try {
			if(fileExistsAndContainsData()) new FileOutputStream(SAVED_DATA).close(); //if contacts already contains data, clears the file for fresh write.
			
			fwrite = new FileWriter(SAVED_DATA, true);
			out = new PrintWriter(fwrite);
			
			//write contact data
			for(Contact contact : contactsMap.values()){
				out.write(contact.getClass() + "," + contact.getId()+ "," + contact.getName() + "," + contact.getNotes());
				out.write(System.getProperty("line.separator"));
			}
			// write meeting data
			for(Meeting meeting : meetingsMap.values()){
				out.write(meeting.getClass() + "," + meeting.getId()); 
				
				Calendar date = meeting.getDate();
				String year = dateFormat.format(date.getTime());
				out.write("," + year);
				
				Set<Contact> meetingParticipants = meeting.getContacts();
				for(Contact contact : meetingParticipants){
					out.write("," + contact.getId());
				}
				
				if(meeting instanceof PastMeeting) {
					PastMeeting pastMeetingTemp = (PastMeeting) meeting;
					out.write("," + pastMeetingTemp.getNotes());
				}
				
				out.write(System.getProperty("line.separator"));
			}
			//write generator id
			out.write(idGenerator.getClass() + "," + idGenerator.getId());
		} catch (FileNotFoundException ex) {
			System.out.println("Cannot write to file " + SAVED_DATA + ".");
		} catch (IOException ex){
			System.out.println(ex);
		}
		finally {
			out.close();
		}
	}
	
	
		
	/**
	 * Tests if contacts in Set are within user's contactsMap
	 *
	 *@param set of contacts to be tested
	 *@return true if all contacts in set are in contact map
	 *
	*/
	private boolean allContactsInSet(Set<Contact> contacts){
		for(Contact contact: contacts){
			if(contactsMap.get(contact.getId()) == null) return false;
		}
		return true;
	}
	
	/**
	 * Tests if contact attending specific meeting
	 *
	 *@param the Contact being checked
	 *@return true if contact is attending meeting
	 *
	*/
	private boolean contactInMeeting(Set<Contact> contacts, Contact contact){
		for(Contact contactOther: contacts){
			if(contactOther.equals(contact)) return true;
		}
		return false;
	}
	
	
	
	/**
	 * Tests whether data file exists and contains data
	 *
	 *@return true if date file exists and contains data
	 *
	*/
	private boolean fileExistsAndContainsData(){
		if(SAVED_DATA.exists() && SAVED_DATA.length() > 0){
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 *
	 *Reloads saved data from data file.  
	 *
	 *Note that order of contacts will differ in data file between flushes so even though data the same, it is being re-added in different order.  This is because it is being written into data file from a hashmap and then re-loaded from array so order of iteration differs.  There is no data corruption however.
	 *
	*/
	private void loadSavedData(){
	BufferedReader in = null;
		try{	
			in = new BufferedReader(new FileReader(SAVED_DATA));
			String line;
			while ((line = in.readLine()) != null) {
				String[] lineArray = line.split(",");
				
				//Reload Contact data
				if(lineArray[0].contains("ContactImpl")){
					String name = lineArray[2];
					String notes = lineArray[3];
					int contactId = Integer.parseInt(lineArray[1]);
					
					Contact contact = new ContactImpl(name, notes, contactId);
					//contactsList.add(contact);
					contactsMap.put(contactId, contact);
				}
				
				//Reload FutureMeeting data
				if(lineArray[0].contains("FutureMeetingImpl")) {			
					int meetingId = Integer.parseInt(lineArray[1]);
					int meetingYear = Integer.parseInt(lineArray[2]);
					int meetingMonth = Integer.parseInt(lineArray[3])-1;// minus 1 because Calendar counts date from zero
					int meetingDay = Integer.parseInt(lineArray[4]);
					Calendar meetingDate = new GregorianCalendar(meetingYear, meetingMonth, meetingDay);
					 
					Set<Contact> meetingParticipants = new HashSet<Contact>();
					for(int i = 5; i < lineArray.length; i++){
					//starts at 5 as meetingParticipants data starts at array index 5
						int meetingParticipantId = Integer.parseInt(lineArray[i]);
						meetingParticipants.add(contactsMap.get(meetingParticipantId));
					}
					
					Meeting meeting = new FutureMeetingImpl(meetingParticipants, meetingDate, meetingId);
					meetingsMap.put(meeting.getId(), meeting);
				}
				
				//Some code reuse between above block and this.  Needs to be refactored to combine more elements of Future and Past meeting reloading. 
				if(lineArray[0].contains("PastMeetingImpl")){
					int meetingId = Integer.parseInt(lineArray[1]);
					int meetingYear = Integer.parseInt(lineArray[2]);
					int meetingMonth = Integer.parseInt(lineArray[3])-1;// minus 1 because Calendar counts date from zero
					int meetingDay = Integer.parseInt(lineArray[4]);
					String meetingNotes = lineArray[lineArray.length-1];
					Calendar meetingDate = new GregorianCalendar(meetingYear, meetingMonth, meetingDay);
					 Set<Contact> meetingParticipants = new HashSet<Contact>();
					
					//for loop starts at 5 as meetingParticipants data starts at array index 5. Minus 1 because last array index contains the notes
					for(int i = 5; i < lineArray.length-1; i++){
						int meetingParticipantId = Integer.parseInt(lineArray[i]);
						meetingParticipants.add(contactsMap.get(meetingParticipantId));
					}
					
					Meeting meeting = new PastMeetingImpl(meetingParticipants, meetingDate, meetingId, meetingNotes);
					meetingsMap.put(meeting.getId(), meeting);			
				}
				
				if(lineArray[0].contains("MakeIdImpl")) {
					idGenerator.reInitialiseId(Integer.parseInt(lineArray[1])); 
				}
					
			}
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} catch (IOException ex){
			ex.printStackTrace();
		} finally{
			closeReader(in);
		}
	}
	private void closeReader(Reader reader){
		try{
		if(reader != null){
			reader.close();
		}
		} catch(IOException ex){
			ex.printStackTrace();
		}
	}
	
	
	
	/**
	 * Check to see if two Calender object's date's match
	 *
	 *@param two Calendar objects
	 *@return true if dates natch
	 *
    */
	private boolean daysMatch(Calendar dateOne, Calendar dateTwo){
		boolean sameDay = (dateOne.get(Calendar.YEAR) == dateTwo.get(Calendar.YEAR) && dateOne.get(Calendar.DAY_OF_YEAR) == dateTwo.get(Calendar.DAY_OF_YEAR));
		return sameDay;
	}
	

}