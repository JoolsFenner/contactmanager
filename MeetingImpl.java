import java.util.Set;
import java.util.HashSet;
import java.util.Calendar;
import java.util.GregorianCalendar;


/**
 * Implements the Meeting interface
 *
 * @author Julian Fenner
*/

public class MeetingImpl implements Meeting {
	private int meetingId;
	private Calendar meetingDate;
	private Set<Contact> meetingParticipants;
	
	public MeetingImpl(Set<Contact> meetingParticipants, Calendar calendar, int meetingId){
		if(meetingParticipants.isEmpty()){
			throw new IllegalArgumentException("Error. There are no contacts for this meeting");
		}
		
		this.meetingParticipants = meetingParticipants;
		this.meetingId = meetingId;
		this.meetingDate = calendar;
		
	}
	
	@Override
	public int getId(){
		return meetingId;
	}
	
	@Override
	public Calendar getDate(){
		return meetingDate;
	}
	
	@Override
	public Set<Contact> getContacts(){
		return meetingParticipants;
	}
}