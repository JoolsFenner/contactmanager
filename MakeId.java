/**
 *Used by all classes in ContactManager to assign a new unique Id. Placed here so that id creation logic is not inside Contact and Meeting constuctor.  This is so id can be loaded either from here (when creating a new instance) or reloaded from CSV when data already exists.
*/ 


public interface MakeId{
	/**
	 * 
	 * Returns a new unique Id
	 *
	*/
	int createNewId();
	
	/**
	 *
	 *	Used when Contact Manager data saved and Id static field needs to reinitialise to saved state 
	 *
	*/
	void reInitialiseId(int savedId);
	
	/**
	 *
	 * Shows the next id to be allocated
	*/
	int getId();
}	