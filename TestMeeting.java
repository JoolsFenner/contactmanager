import org.junit.*;
import static org.junit.Assert.*;
import org.junit.rules.ExpectedException;
import java.util.Set;
import java.util.HashSet;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class TestMeeting {
	Meeting birkbeckMeeting;
	
	private Contact julian;
	private Contact david;
	private Contact linda;
	Set<Contact> meetingParticipants;
	Set<Contact> meetingParticipants2;
	
	Calendar meetingDate;
	Calendar meetingDate2;
	
	@Before
	public void buildUp() {
		julian = new ContactImpl("Julian Fenner", 1);
		david = new ContactImpl("David Fenner", 2);
		linda = new ContactImpl	("Linda Florence", 3);
		
		meetingParticipants = new HashSet<Contact>();
		meetingParticipants.add(julian);
		meetingParticipants.add(david);
		meetingParticipants.add(linda);
				
		meetingDate = new GregorianCalendar(2014,7,1);
		meetingDate2 = new GregorianCalendar(2014,9,14);
		
		birkbeckMeeting = new MeetingImpl(meetingParticipants, meetingDate, 1);
		
	}
	
	@Test
	public void getIdTest() {
	//establishes meetings are being successfully created by testing side effect i.e next meeting will have an id of 2.
		int expectedId = 1;
		int outputId = birkbeckMeeting.getId();
		assertEquals(expectedId, outputId);
	}
	
	@Test 
	public void getDate(){
		Calendar expectedDate = new GregorianCalendar(2014,7,1);
		Calendar outputDate = birkbeckMeeting.getDate();
		assertEquals(expectedDate, outputDate);	
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void emptyContactSet(){
		meetingParticipants2 = new HashSet<Contact>();
		birkbeckMeeting = new MeetingImpl(meetingParticipants2, meetingDate2, 1);
	}
	
}