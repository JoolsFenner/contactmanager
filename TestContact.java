import org.junit.*;
import static org.junit.Assert.*;


public class TestContact {
	
	private Contact julian;
	
	@Before
	public void buildUp() {
		julian = new ContactImpl("Julian Fenner", 1);
	}
	
	@Test
	public void createContact() {
		Contact david = new ContactImpl("David", 2);
		String expectedName = "David";
		String outputName = david.getName();
		assertEquals(expectedName, outputName);
	}
			
	@Test
	public void addNotesTest(){
		String notes1 = "Julian Fenner is studying Computer Science At Birkbeck.  ";
		String notes2 = "He is currently interested in Java programming.  ";
		julian.addNotes(notes1);
		julian.addNotes(notes2);
		String expectedNotes = notes1 + notes2;
		String outputNotes = julian.getNotes();
		assertEquals(expectedNotes, outputNotes);
	}

}