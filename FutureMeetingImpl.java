import java.util.Set;
import java.util.Calendar;

/**
 * Implements the FutureMeeting interface
 *
 * @author Julian Fenner
*/
 public class FutureMeetingImpl extends MeetingImpl implements FutureMeeting {

	public FutureMeetingImpl(Set<Contact> meetingParticipants, Calendar calendar, int meetingId) {	
		super(meetingParticipants, calendar, meetingId);
	}

}